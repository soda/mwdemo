<?php
// ==========================================================================
// LocalSettings for your Toolforge hosted wiki.
//
// THIS FILE IS PUBLICLY VIEWABLE!!
// Do not put any secrets in it. Put those in PrivateSettings.php.
// ==========================================================================

// Protect against web entry
if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}

// PrivateSettings.php has non-public secrets (passwords etc.)
require_once __DIR__ . '/PrivateSettings.php';
// $wgSecretKey in PrivateSettings.php
// $wgUpgradeKey in PrivateSettings.php
// $wgOAuthClientId in PrivateSettings.php
// $wgOAuthClientSecret in PrivateSettings.php

// ==========================================================================
// CHANGE THIS FOR YOUR TOOL!
$wgSitename = 'MediaWiki Toolforge demo';
// ==========================================================================

$wlToolName = explode( '.', posix_getpwuid( posix_getuid() )[ 'name' ], 2 )[ 1 ];
$wgServer = "https://{$wlToolName}.toolforge.org";

$wgScriptPath = '/w';
$wgArticlePath = '/wiki/$1';
$wgResourceBasePath = $wgScriptPath;

$wgMetaNamespace = 'Project';
$wgFavicon = "$wgScriptPath/images/0/04/Favicon.svg";
$wgLogos = [
	'svg' => "$wgScriptPath/images/6/6f/Logo.svg",
	'icon' => "$wgScriptPath/images/6/6f/Logo.svg",
];

$wgEnableEmail = false;
$wgEnableUserEmail = true;
$wgEmergencyContact = 'nobody@🌻.invalid';
$wgPasswordSender = 'nobody@🌻.invalid';
$wgEnotifUserTalk = false;
$wgEnotifWatchlist = false;
$wgEmailAuthentication = true;

$wgAllowUserCss = true;
$wgAllowUserJs = true;

// ==========================================================================
// Database settings
/**
 * Read database credentials from the tool's $HOME/replica.my.cnf file.
 * @return array [ "user" => "...", "password" => "..." ]
 */
function wlDbCredentials() {
	$uid = posix_getuid();
	$userinfo = posix_getpwuid( $uid );
	$home = $userinfo[ 'dir' ];
	$cnf = "{$home}/replica.my.cnf";
	$settings = parse_ini_file( $cnf, true, INI_SCANNER_RAW );
	if ( $settings === false ) {
		throw new RuntimeException( "Error reading {$cnf}" );
	}
	return $settings['client'];
}

$wgDBtype = 'mysql';
$wgDBserver = 'tools.db.svc.eqiad.wmflabs';
$wlCreds = wlDbCredentials();
$wgDBuser = $wlCreds[ 'user' ];
$wgDBpassword = $wlCreds[ 'password' ];
$wgDBname = "{$wgDBuser}__mediawiki";
$wgDBprefix = '';
$wgDBTableOptions = 'ENGINE=InnoDB, DEFAULT CHARSET=binary';
// ==========================================================================

// ==========================================================================
// Cache settings
$wgMainCacheType = CACHE_ACCEL;
$wgSessionCacheType = CACHE_DB;
$wgMemCachedServers = [];
// Keep sessions for 24 hours
$wgObjectCacheSessionExpiry = 86400;
$wgCacheDirectory = '/tmp';  // This is ephemeral storage within the container
// ==========================================================================

$wgEnableUploads = false;
$wgPingback = false;

$wgShowExceptionDetails = true;

$wgShellLocale = 'C.UTF-8';
$wgLanguageCode = 'en';
$wgLocaltimezone = 'UTC';

// Changing this will log out all existing sessions.
$wgAuthenticationTokenVersion = '1';

// For attaching licensing metadata to pages, and displaying an
// appropriate copyright notice / icon. GNU Free Documentation
// License and Creative Commons licenses are supported so far.
$wgRightsPage = ''; # Set to the title of a wiki page that describes your license/copyright
$wgRightsUrl = 'https://creativecommons.org/licenses/by-sa/3.0/';
$wgRightsText = 'Creative Commons Attribution-ShareAlike';
$wgRightsIcon = "$wgResourceBasePath/resources/assets/licenses/cc-by-sa.png";

// Path to the GNU diff3 utility. Used for conflict resolution.
$wgDiff3 = "/usr/bin/diff3";

$wgMemoryLimit = '2G';

$wgNamespacesWithSubpages[NS_MAIN] = true;
$wgNamespacesWithSubpages[NS_MEDIAWIKI] = true;
$wgNamespacesWithSubpages[NS_TEMPLATE] = true;

// ==========================================================================
// Enabled skins.
wfLoadSkin( 'Vector' );

$wgDefaultSkin = "vector-2022";

// ==========================================================================
// Enabled extensions. Most of the extensions are enabled by adding
// wfLoadExtension( 'ExtensionName' );
// Check specific extension documentation for more details.
wfLoadExtension( 'CodeEditor' );
wfLoadExtension( 'Gadgets' );
wfLoadExtension( 'QuickInstantCommons' );
wfLoadExtension( 'SyntaxHighlight_GeSHi' );
wfLoadExtension( 'TemplateData' );
wfLoadExtension( 'TemplateStyles' );
wfLoadExtension( 'VisualEditor' );
wfLoadExtension( 'WikiEditor' );

wfLoadExtension( 'PluggableAuth' );
$wgPluggableAuth_EnableAutoLogin = false;
$wgPluggableAuth_EnableLocalLogin = false;

$wgGroupPermissions['*']['edit'] = false;
$wgGroupPermissions['*']['createaccount'] = false;
$wgGroupPermissions['*']['autocreateaccount'] = true;

wfLoadExtension( 'WSOAuth' );
$wgOAuthUri = 'https://meta.wikimedia.org/w/index.php?title=Special:OAuth';
// $wgOAuthClientId in PrivateSettings.php (not really secret)
// $wgOAuthClientSecret in PrivateSettings.php
$wgPluggableAuth_Config['wikimedia'] = [
	'plugin' => 'WSOAuth',
	'buttonLabelMessage' => 'wikimedia-login-button-label',
	'data' => [
		'type' => 'mediawiki',
		'uri' => $wgOAuthUri,
		'clientId' => $wgOAuthClientId,
		'clientSecret' => $wgOAuthClientSecret,
		'migrateUsersByUsername' => true,
	],
];

wfLoadExtension( 'ParserFunctions' );
$wgPFEnableStringFunctions = true;

wfLoadExtension( 'Scribunto' );
$wgScribuntoDefaultEngine = 'luastandalone';
$wgScribuntoUseGeSHi = true;
$wgScribuntoUseCodeEditor = true;

wfLoadExtension( 'CodeMirror' );
$wgDefaultUserOptions['usecodemirror'] = 1;

wfLoadExtension( 'Interwiki' );
$wgGroupPermissions['sysop']['interwiki'] = true;

wfLoadExtension( 'JsonConfig' );
$wgJsonConfigEnableLuaSupport = true;
$wgJsonConfigModels['Tabular.JsonConfig'] = 'JsonConfig\JCTabularContent';
$wgJsonConfigs['Tabular.JsonConfig'] = [
        'namespace' => 486,
        'nsName' => 'Data',
        // page name must end in ".tab", and contain at least one symbol
        'pattern' => '/.\.tab$/',
        'license' => 'CC0-1.0',
        'isLocal' => false,
];
$wgJsonConfigModels['Map.JsonConfig'] = 'JsonConfig\JCMapDataContent';
$wgJsonConfigs['Map.JsonConfig'] = [
        'namespace' => 486,
        'nsName' => 'Data',
        // page name must end in ".map", and contain at least one symbol
        'pattern' => '/.\.map$/',
        'license' => 'CC0-1.0',
        'isLocal' => false,
];
$wgJsonConfigInterwikiPrefix = 'commons';
$wgJsonConfigs['Tabular.JsonConfig']['remote'] = [
        'url' => 'https://commons.wikimedia.org/w/api.php'
];
$wgJsonConfigs['Map.JsonConfig']['remote'] = [
        'url' => 'https://commons.wikimedia.org/w/api.php'
];
