/* Any JavaScript here will be loaded for all users on every page load. */

/**
 * Replace <span class="CURRENTLOGGEDUSER">...</span> with username.
 */
mw.hook( "wikipage.content" ).add( function () {
	if ( !Array.from || !document.append ) { return; }
	document.querySelectorAll( "span.CURRENTLOGGEDUSER" ).forEach(
		function ( e ) {
			e.textContent = mw.config.get( 'wgUserName' ) || e.textContent;
		}
	);
} );
