'''Initial setup''' instructions for MediaWiki on Toolforge.

If you are reading this on-wiki, congratulations on getting this far! There are just a few more steps that you will need to take to finish the initial setup of your demo wiki.

== OAuth registration ==
You should now have a functional wiki, except you probably cannot quite figure out how to login. The [[gitlab:toolforge-repos/mwdemo/-/blob/main/etc/LocalSettings.php|LocalSettings.php]] that has been installed has configured MediaWiki to expect to use [[mw:OAuth|]] to authenticate users. Before that will work however, you will need to register your wiki as a new OAuth application and add a few values to your <code>$HOME/etc/PrivateSettings.php</code> file.

To register a new OAuth application, submit the form at [[meta:Special:OAuthConsumerRegistration/propose/oauth1a]]:

; Application name
: <code>{{SERVERNAME}}</code>
; Consumer version
: <code>1.0</code>
; Application description
: A description of the purpose of your wiki.
; OAuth "callback" URL
: <code>https<nowiki/>://{{SERVERNAME}}/w/index.php?title=Special:PluggableAuthLogin</code>
; Applicable project
: <code>*</code>
; Types of grants being requested
: Select the ''User identity verification only, no ability to read pages or act on a user's behalf.'' option

Copy the ''consumer token'' and ''secret token'' values from the server response into your <code>$HOME/etc/PrivateSettings.php</code> file as the <syntaxhighlight lang=php inline>$wgOAuthClientId</syntaxhighlight> and <syntaxhighlight lang=php inline>$wgOAuthClientSecret</syntaxhighlight> settings.

You should now be able to <span class="plainlinks">[{{canonicalurl:Special:UserLogin|returnto={{urlencode:{{FULLPAGENAME}}|QUERY}}}} login]</span> using the OAuth grant. Until the grant request is approved by an OAuth admin on meta you will only be able to authenticate as the wiki user who requested the grant.

== Making your user an admin ==
An adminstrator account was created when MediaWiki was installed, but that account uses a local password and your wiki is now configured to authenticate folks with OAuth instead. You probably do still want to have an administrator account though, so let's make your OAuth account an admin using [[mw:Manual:CreateAndPromote.php|createAndPromote.php]].

# <span class="plainlinks">[{{canonicalurl:Special:     UserLogin|returnto={{urlencode:{{FULLPAGENAME}}|QUERY}}}} Login]</span> if you have not already to create your account.
# <code>ssh login.toolforge.org</code>
# <code>become {{#explode:{{SERVERNAME}}|.|0}}</code>
# <code>webservice php7.4 shell -- bin/mwmaint createAndPromote --force --sysop --bureaucrat --interface-admin '<span class="CURRENTLOGGEDUSER"><username></span>'</code>

== See also ==
* [[Project:Mwdemo]]
