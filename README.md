MediaWiki on Toolforge demo
===========================

This repo contains helper scripts and other information to assist in running
a MediaWiki wiki on the Toolforge hosting service.

The wiki will be setup with a number of useful extensions and OAuth
authentication with the Wikimedia wikis.

Usage
-----
It is recommended that you fork this repo to start your own MediaWiki on
Toolforge. This will let you use the forked repo to track your own
configuration changes.

### Installation
```
$ ssh login.toolforge.org
$ become <tool>
$ git init
$ git checkout -b main
$ git remote add origin <repository-url>
$ git pull origin main
$ git branch --set-upstream-to=origin/main main
$ webservice --cpu 1 --mem 2G php7.4 shell -- bash -x bin/install.sh
  # This step will take about 15 minutes to complete.
  # Cloning mediawiki/core is likely to be the slowest part of the install.
$ webservice start
$ toolforge-jobs load etc/jobs.yaml
```
This will have created the wiki and initialized its configuration. There are
a few more manual steps needed to complete the OAuth setup. See [[Help:Initial
setup]] in your wiki for more information.

License
-------
[GPL-2.0-or-later](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
