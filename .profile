# default umask rw-rw-r--
umask 0002

export PATH=$PATH:$HOME/bin

alias ls='command ls -bCF --color=auto'
alias ll='ls -l'
alias k=kubectl
alias jobs=toolforge-jobs
