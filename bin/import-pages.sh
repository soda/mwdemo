#!/usr/bin/env bash
# Import content into the wiki.
#
# SPDX-License-Identifier: GPL-2.0-or-later
set -euo pipefail

ETC_DIR=$HOME/etc
WIKI_DIR=$HOME/public_html/w

function mwmaint() {
    pushd $WIKI_DIR;
    php maintenance/run.php "$@"
    popd
}

cd $ETC_DIR/content/pages
for src in $(find . -type f | cut -d/ -f2-); do
    # Convert file path to title by:
    # - removing any '.mediawiki' suffix
    # - changing `/` to `:`
    # - changing `_` to ` `
    title=$(echo ${src%.mediawiki} | tr '/_' ': ')
    mwmaint edit \
        --summary='import-pages.sh run' \
        --no-rc \
        "${title}" < "$ETC_DIR/content/pages/${src}"
done
mwmaint rebuildall
