#!/usr/bin/env bash
# Update git clones, composer libraries, and database.
#
# SPDX-License-Identifier: GPL-2.0-or-later
set -euxo pipefail

function die {
    printf >&2 'Update error: %s\n' "$1"
    exit 1
}

cd $HOME/public_html/w/ || die 'No MediaWiki directory'

for dir in . extensions/*/ skins/*/; do
    HEAD=$(git -C "$dir" rev-parse --abbrev-ref HEAD)
    git -C "$dir" pull --rebase origin $HEAD ||
    {
        git -C "$dir" checkout @ .
        die "Git pull for $dir failed; git checkout returned $?"
    }
    git -C "$dir" submodule update --init --recursive
done

composer update --no-interaction --no-dev --optimize-autoloader ||
    die 'Composer update failed'

php maintenance/run.php update --quick || die 'Update.php failed'

php maintenance/run.php runJobs || die 'RunJobs.php failed'
