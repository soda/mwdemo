#!/usr/bin/env bash
# Install and initialize a MediaWiki instance
#
# SPDX-License-Identifier: GPL-2.0-or-later
set -euo pipefail

BIN_DIR=$HOME/bin
ETC_DIR=$HOME/etc
WIKI_DIR=$HOME/public_html/w
DB_USER=$(grep user $HOME/replica.my.cnf | awk '{print $3}')
DB_PASS=$(grep password $HOME/replica.my.cnf | awk '{print $3}')
TOOL=$(whoami | cut -d. -f2)

function git_clone() {
    # Only clone the HEAD branch to speed things up
    git clone --single-branch --recurse-submodules \
        https://gerrit.wikimedia.org/r/${1}
}

function mwmaint() {
    pushd $WIKI_DIR;
    php maintenance/run.php "$@"
    popd
}

if [[ -d $WIKI_DIR ]]; then
    echo "ERROR: $WIKI_DIR exists." 1>&2
    echo "ERROR: cowardly refusing to continue." 1>&2
    exit 1
fi

# The mediawiki/core repo is big and slow to clone on the NFS $HOME in
# Toolforge. Speed things up a bit by first cloning to a local directory and
# then moving the clone to $HOME.
git clone --single-branch https://gerrit.wikimedia.org/r/mediawiki/core /tmp/w
mkdir -p $WIKI_DIR
rmdir $WIKI_DIR
mv /tmp/w $WIKI_DIR

cd $WIKI_DIR/skins
git_clone mediawiki/skins/Vector

cd $WIKI_DIR/extensions
git_clone mediawiki/extensions/CodeEditor
git_clone mediawiki/extensions/CodeMirror
git_clone mediawiki/extensions/Gadgets
git_clone mediawiki/extensions/InputBox
git_clone mediawiki/extensions/Interwiki
git_clone mediawiki/extensions/JsonConfig
git_clone mediawiki/extensions/ParserFunctions
git_clone mediawiki/extensions/PluggableAuth
git_clone mediawiki/extensions/QuickInstantCommons
git_clone mediawiki/extensions/Scribunto
git_clone mediawiki/extensions/SyntaxHighlight_GeSHi
git_clone mediawiki/extensions/TemplateData
git_clone mediawiki/extensions/TemplateStyles
git_clone mediawiki/extensions/VisualEditor
git_clone mediawiki/extensions/WSOAuth
git_clone mediawiki/extensions/Wikibase
git_clone mediawiki/extensions/WikiEditor

cd $WIKI_DIR
cp composer.local.json-sample composer.local.json
composer update --no-interaction --no-dev --optimize-autoloader

# Run the MediaWiki installer to initialize the wiki's database on ToolsDB.
# The new wiki will have a [[User:Admin]] with the same password as the tool's
# database user from $HOME/replica.my.cnf.
OLD_UMASK=$(umask); umask 0007
mwmaint install \
    --dbname="${DB_USER}__mediawiki" \
    --dbserver="tools.db.svc.wikimedia.cloud" \
    --dbuser="${DB_USER}" \
    --dbpass="${DB_PASS}" \
    --server="https://${TOOL}.toolforge.org" \
    --scriptpath="/w" \
    --pass="${DB_PASS}" \
    "${TOOL}" \
    "Admin"
umask $OLD_UMASK

# Replace installer generated LocalSettings with our own
rm $WIKI_DIR/LocalSettings.php
ln -s $ETC_DIR/LocalSettings.php $WIKI_DIR/LocalSettings.php

# Create initial PrivateSettings
OLD_UMASK=$(umask); umask 0007
SECRET_KEY=$(php -r 'echo substr(bin2hex(random_bytes(32)), 0, 64);') \
UPGRADE_KEY=$(php -r 'echo substr(bin2hex(random_bytes(8)), 0, 16);') \
${BIN_DIR}/expand.sh <${ETC_DIR}/PrivateSettings.php.tmpl >${ETC_DIR}/PrivateSettings.php
umask $OLD_UMASK

# Copy files into MediaWiki locations
cp -r $ETC_DIR/content/images $WIKI_DIR

# Load interwiki data
mwmaint sql "$ETC_DIR/content/sql/interwiki.sql"

# Run database update to add any tables needed by the installed extensions
mwmaint update --quick

# Import wiki pages
$BIN_DIR/import-pages.sh
